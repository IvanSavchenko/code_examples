"""10 GB TASK."""
import random
import os
import csv
from cStringIO import StringIO

# Initialize constants,
LENGTH_LIST = [1024, 1024, 1024, 10]
TEN_GIGABYTES = long(reduce(lambda x, y: x*y, LENGTH_LIST))

FILTER_FILE_NAME = '10gn.csv'
RESULT_FILE_NAME = 'result.csv'
# Lets make buffer 50mb.
BUFFER_SIZE = 1024 * 1024 * 50

CSV_LINE_TEMPLATES = [['10', '20', '30', 'spam', '60'],
                      ['20', '40', 'python', '50'],
                      ['100', '50', 200, 'friday']]
CSV_QUOTECHAR = ';'
FILTER_KEY = 'python'


# Making function for reading file with 10GB length,
def read_big_file(filename):
    """Reading big csv file using generator."""
    with open(filename, 'rb') as file_to_read:
        csv_rows = csv.reader(file_to_read, quotechar=CSV_QUOTECHAR)
        for row in csv_rows:
            yield row


# Ok. Lets make 10GB file,
def make_10gb_file(filename):
    """Writing file until he reach 10GB size."""
    with open(filename, 'wb') as opened_file:
        csv_writer = csv.writer(opened_file, quotechar=CSV_QUOTECHAR)

        # Filling up our filter file while his size will be
        # greater than we set up.
        while opened_file.tell() < TEN_GIGABYTES:
            random_row_index = random.randint(0, len(CSV_LINE_TEMPLATES)-1)
            csv_writer.writerow(CSV_LINE_TEMPLATES[random_row_index])


def write_buffer_to_result_file(result_file, buffer_file):
    """Hook to write buffered data into the file."""
    with open(result_file, 'ab') as file_to_write:
        # Getting our cursor to the top of buffer file.
        buffer_file.seek(0)

        # Creating generator of rows to write
        rows_to_write = csv.reader(buffer_file, quotechar=CSV_QUOTECHAR)

        # Write buffered rows to result csv file.
        csv_writer = csv.writer(file_to_write, quotechar=CSV_QUOTECHAR)
        for row in rows_to_write:
            csv_writer.writerow(row)


# Now its time for filtering.
def filter_file(filter_file, result_file, buffer_size, filter_key):
    """Filtering csv file uscing Counter."""
    buffer_file = StringIO()

    # Check if we already have result file.
    if os.path.isfile(result_file):
        os.remove(result_file)

    for row in read_big_file(filter_file):
        # Checking if our buffer not above buffer size we've set up.
        # If it is, write buffer into the result file.
        if buffer_file.tell() > buffer_size:
            write_buffer_to_result_file(result_file, buffer_file)

            # Refresh our buffer.
            buffer_file.close()
            buffer_file = StringIO()

        # Now lets move on and write founded row.
        # If its in filter list, of course!
        if filter_key in row:
            csv_writer = csv.writer(buffer_file, quotechar=CSV_QUOTECHAR)
            csv_writer.writerow(row)

    # Cheking if we still have something in buffer after iteration stops.
    # In that case we need to write tail to the file too.
    else:
        if buffer_file.tell() != 0:
            write_buffer_to_result_file(result_file, buffer_file)

if __name__ == '__main__':
    make_10gb_file(FILTER_FILE_NAME)

    filter_file(filter_file=FILTER_FILE_NAME,
                result_file=RESULT_FILE_NAME,
                buffer_size=BUFFER_SIZE,
                filter_key=FILTER_KEY)